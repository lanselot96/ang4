1. install or update: node >= 8.4.0, npm >= 5.3.0, angular cli >= 1.0 (better 1.4...)
2. `ng new --ng4 angular-hello-world` - creating directory `angular-hello-world` with ng4 files, then `cd angular-hello-world/`  
3. Run `ng serve` 
4. In root folder `ng g c hello-world` where g = generate, c = component, 'hello-world' is a component name
базовый компонент имеет две части:
  1. декоратор
  2. определяющий класс
при использовании `ng g c ...` в файл app.modules.ts добавляется `import` & `declaration`
`ng serve` автоматически компилирует *.ts k *.js (TypeScript to JavaScript)
`@angular/core` это модуль, из которого в hello-world.component.ts берутся два объекта Component & OnInit
`import { things } from wherever` здесь `things` деструкторы (зерез запятую)
`import` это аналог `require` (node) , `use` (Laravel)
`@Component({...})` - это декораторы (можно считать методатой добавленной к нашему коду)
когда используется `@Component` в HelloWorld классе - мы декорируем класс как компонент
чтобы использовать teg <app-hello-world> в html мы указали в декораторе селектор этого компонента
