import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  template: `
  <p>
    Like this or like lower by using templateUrl (Lower priory then templateUrl)
  </p>
  `,
  templateUrl: './hello-world.component.html',
  styleUrls: [
    './hello-world.component.css'
  ]
})
export class HelloWorldComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
